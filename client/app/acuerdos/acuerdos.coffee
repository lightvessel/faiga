'use strict'

angular.module 'faigaApp'
.config ($stateProvider) ->
  $stateProvider.state 'acuerdos',
    parent: 'posts'
    url: '/acuerdos-salariales'
    controller: 'AcuerdosCtrl'
