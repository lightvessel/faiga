'use strict'

angular.module 'faigaApp'
.controller 'AcuerdosCtrl', ($scope, Section, FaigaApi) ->
  $scope.setSection new Section 'Acuerdos',
    'descripcion',
    'acuerdosSalariales',
    '/assets/images/acuerdos.jpg'

  FaigaApi.get 'acuerdos',
    (response)->
      $scope.initPosts(response.data.news)

