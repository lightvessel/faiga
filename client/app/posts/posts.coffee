'use strict'

angular.module 'faigaApp'
.config ($stateProvider) ->
  $stateProvider.state 'posts',
    parent: 'main_layout'
    abstract: true
    templateUrl: 'app/posts/posts.html'
    controller: 'PostsCtrl'
