'use strict'

angular.module 'faigaApp'
.filter 'DateFormatter', ->
  (date) ->
    if(!date) then return ""
    tDate = new Date(date.replace(/\-/g, '/'))
    "#{tDate.getDate()}/#{tDate.getMonth()+1}/#{tDate.getFullYear()}"
