'use strict'

angular.module 'faigaApp'
.controller 'PostsCtrl', ($scope) ->
  $scope.setSection = (section) ->
    $scope.section = section
    $scope.setTitle($scope.section.title)

  $scope.news = {posts:[], showMoreButton:false}

  $scope.page = 0

  $scope.$on '$stateChangeStart',
    ()->
      $scope.page = 0

  $scope.postsPerPage = 20

  $scope.initPosts = (posts)->
    if($scope.page == 0)
      $scope.featured = _.find(posts, (e)-> e.featuredResource)
      $scope.news.posts = posts
    else
      _.each(posts, (e)-> $scope.news.posts.push(e))

    if(posts.length < $scope.postsPerPage)
      $scope.news.showMoreButton = false

  $scope.setPostRequestAndStart = (specificRequest) ->
    $scope.postRequest = specificRequest
    $scope.postRequest($scope.page, $scope.postsPerPage)
    $scope.news.showMoreButton=true

  $scope.nextPage = ()->
    $scope.page += 1
    $scope.postRequest($scope.page * $scope.postsPerPage, $scope.postsPerPage)
