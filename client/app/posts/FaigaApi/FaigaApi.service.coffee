'use strict'

angular.module 'faigaApp'
.service 'FaigaApi', ($http, $q)->
  API_BASE = 'http://www.faiga.com/api/v1/'
  {
    get: (sectionName, callback, start=0,max=20)->
      $http.get("#{API_BASE}news/section/fromSectionName/#{sectionName}/#{start}/#{max}")
      .success (data)-> callback(data)
    getPost: (id, callback)->
      $http.get("#{API_BASE}news/fromId/#{id}")
      .success (data)-> callback(data)
    findUgarPosts: (callback, start=0,max=20)->
      $http.get("#{API_BASE}news/section/subsection/fromId/4/10/#{start}/#{max}")
      .success (data)-> callback(data)
    findAsesorias: (callback)->
      $http.get("#{API_BASE}asesorias")
      .success (data)-> callback(data)
    findEventos: (callback)->
      $http.get("#{API_BASE}eventos/list")
      .success (data)-> callback(data)
    findUgarRegiones: (callback)->
      $http.get("#{API_BASE}ugar/regiones")
      .success (data)-> callback(data)
    findInstitucional: (callback)->
      $http.get("#{API_BASE}institucional")
      .success (data)-> callback(data)
    findVideos: (callback)->
      $http.get("#{API_BASE}video")
      .success (data)-> callback(data)
    findLicitaciones: (callback)->
      $http.get("#{API_BASE}custom_page/12")
      .success (data)-> callback(data)
    #HACK - La API no acepta OPTIONS y falla el pre-flight, se manda el type como text/plain
    postContacto: (contacto, callback)->
      $http.post("#{API_BASE}contacto", contacto, {headers:{'Content-Type':'text/plain'}})
      .success ()-> callback()
    findEstablecimiento: (character, callback)->
      $http.get("#{API_BASE}empresas/firstLetter/#{character}")
      .success (data)-> callback(data)
    findProveedores: (character, callback)->
      $http.get("#{API_BASE}empresas/proveedores/firstLetter/#{character}")
      .success (data)-> callback(data)
    findPaisesProvincias: (callback) ->
      $http.get("#{API_BASE}contacto_paises_provincias")
      .success (data) -> callback(data.data)
    findAporteFATIDA: (callback)->
      $http.get("#{API_BASE}custom_page/37")
      .success (data)-> callback(data)
    findAporteSFGB: (callback)->
      $http.get("#{API_BASE}custom_page/38")
      .success (data)-> callback(data)

    #No hay una API bonita de búsquedas, se improvisa un hack llamando a la url generica de búsqueda y filtrando los resultados a mano
    busquedaNotas: (filtroBusqueda={}, callback)  ->
      resultNews = []

      $http.get("#{API_BASE}news/search/#{filtroBusqueda.texto || ''}").then (response) ->
        noticias = response.data.data.news_found

        if(!_.isEmpty filtroBusqueda.tipos)
          _.forEach filtroBusqueda.tipos, (it) ->
            resultNews = resultNews.concat (_.filter noticias, {"NewsSubSection": it} )
        else
          resultNews = noticias

        callback(_.sortBy(resultNews, "DateCreated").reverse())

    #Idem busquedaNotas
    busquedaEmpresas: (filtroBusqueda={}, callback)  ->
      getEmpresas = $http.get("#{API_BASE}empresas/search/#{filtroBusqueda.razon || ''}")
      getProveedores = $http.get("#{API_BASE}empresas/proveedores/search/#{filtroBusqueda.razon || ''}")

      if(filtroBusqueda.buscarProveedores && !filtroBusqueda.buscarEmpresas)
        getProveedores.then (response) -> callback(response.data.data.proveedores)

      else if(filtroBusqueda.buscarEmpresas && !filtroBusqueda.buscarProveedores)
        getEmpresas.then (response) -> callback(response.data.data.empresas)

      else
        $q.all([getProveedores, getEmpresas]).then (response) ->
          result = response[0].data.data.proveedores.concat response[1].data.data.empresas
          callback(_.sortBy(result, "Empresa"))
  }
