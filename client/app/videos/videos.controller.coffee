'use strict'

angular.module 'faigaApp'
.controller 'VideosCtrl', ($scope, Section, FaigaApi, $sce) ->
  $scope.setSection new Section 'Videos',
    'descripcion',
    'videos',
    '/assets/images/videos.jpg'

  $scope.initPosts([])
  FaigaApi.findVideos (response)->
    response.data.video.HTML = $sce.trustAsHtml(response.data.video.HTML)
    $scope.video = response.data.video
