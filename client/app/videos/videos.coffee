'use strict'

angular.module 'faigaApp'
.config ($stateProvider) ->
  $stateProvider.state 'videos',
    url: '/videos'
    parent: 'posts'
    templateUrl: 'app/videos/videos.html'
    controller: 'VideosCtrl'
