'use strict'

angular.module 'faigaApp'
.config ($stateProvider) ->
  $stateProvider.state 'ugar_novedades',
    url: '/novedades'
    parent: 'ugar'
    controller: 'UgarNovedadesCtrl'
