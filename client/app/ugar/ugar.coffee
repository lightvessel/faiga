'use strict'

angular.module 'faigaApp'
.config ($stateProvider) ->
  $stateProvider.state 'ugar',
    url: '/ugar'
    parent: 'posts'
    abstract: true
    templateUrl: 'app/ugar/ugar.html'
    controller: 'UgarCtrl'
