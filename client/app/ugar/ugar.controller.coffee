'use strict'

angular.module 'faigaApp'
.controller 'UgarCtrl', ($scope, Section, FaigaApi) ->
  $scope.setSection new Section 'UGAR',
    'descripcion',
    'ugar',
    '/assets/images/ugar.jpg'
