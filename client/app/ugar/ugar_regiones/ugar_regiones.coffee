'use strict'

angular.module 'faigaApp'
.config ($stateProvider) ->
  $stateProvider.state 'ugar_regiones',
    url: '/regiones'
    parent: 'ugar'
    templateUrl: 'app/ugar/ugar_regiones/regiones.html'
    controller: 'UgarRegionesCtrl'
