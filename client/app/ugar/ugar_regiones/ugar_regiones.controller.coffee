'use strict'

angular.module 'faigaApp'
.controller 'UgarRegionesCtrl', ($scope, Section, FaigaApi, $sce) ->

  $scope.initPosts([])
  FaigaApi.findUgarRegiones (response)->

    # Esto idealmente debería ser hecho con una directiva, pero de por sí la integración con
    # el las páginas custom es bastante manual, y las personas que trabajan de ese lado están más
    # acostumbradas a envíar su lógica con jQuery.
    response.data.regiones.HTML =  $sce.trustAsHtml(response.data.regiones.HTML + """<script>
      $(document).ready(function () {
          $(".ugarRegItem").on('click', function () {
              var $ugarRegDesc = $(this).next(".ugarRegDesc");
              $(this).addClass("ugarRegItemSelected");
              $ugarRegDesc.show();
              $(".ugarRegItem").not($(this)).removeClass("ugarRegItemSelected");
              $(".ugarRegDesc").not($ugarRegDesc).hide();
          });
      });
    </script>""")
    $scope.regiones = response.data.regiones
