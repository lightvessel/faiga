'use strict'

angular.module 'faigaApp'
.config ($stateProvider) ->
  $stateProvider.state 'home',
    parent: 'posts'
    url: '/'
    controller: 'HomeCtrl'
