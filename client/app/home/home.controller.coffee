'use strict'

angular.module 'faigaApp'
.controller 'HomeCtrl', ($scope, Section, FaigaApi) ->
  $scope.setSection new Section 'Home',
    'descripcion',
    'novedades'

  FaigaApi.get 'novedades',
    (response)->
      $scope.initPosts(response.data.news)

