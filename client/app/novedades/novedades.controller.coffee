'use strict'

angular.module 'faigaApp'
.controller 'NovedadesCtrl', ($scope, Section, FaigaApi) ->
  $scope.setSection new Section 'Novedades',
    'descripcion',
    'novedades'

  $scope.setPostRequestAndStart (current, max) ->
    FaigaApi.get 'novedades',
      (response)->
        $scope.initPosts(response.data.news)
    ,current
    ,max
