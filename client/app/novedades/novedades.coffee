'use strict'

angular.module 'faigaApp'
.config ($stateProvider) ->
  $stateProvider.state 'novedades',
    parent: 'posts'
    url: '/novedades'
    controller: 'NovedadesCtrl'
