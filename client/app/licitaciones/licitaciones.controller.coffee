'use strict'

angular.module 'faigaApp'
.controller 'LicitacionesCtrl', ($scope, Section, FaigaApi) ->
  $scope.setSection new Section 'Licitaciones',
    'descripcion',
    'licitaciones',
    '/assets/images/licitaciones.jpg'

  $scope.initPosts([])
  FaigaApi.findLicitaciones (response)->
    $scope.licitaciones = response.data.page

