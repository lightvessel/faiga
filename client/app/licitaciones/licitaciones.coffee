'use strict'

angular.module 'faigaApp'
.config ($stateProvider) ->
  $stateProvider.state 'licitaciones',
    parent: 'posts'
    url: '/licitaciones'
    templateUrl: 'app/licitaciones/licitaciones.html'
    controller: 'LicitacionesCtrl'
