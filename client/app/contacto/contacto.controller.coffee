'use strict'

angular.module 'faigaApp'
.controller 'ContactoCtrl', ($scope, Section, FaigaApi) ->
  $scope.setSection new Section 'Contacto',
    'descripcion',
    'contacto',
    '/assets/images/contacto.jpg'

  $scope.initPosts([])

  FaigaApi.findPaisesProvincias (data) ->
    $scope.provincias = data.provincias
    $scope.paises = data.paises

  $scope.modelo = {
    contacto:{
      "first_name": "",
      "last_name": "",
      "email": "",
      "phone": "",
      "country": "",
      "province": "",
      "company": "",
      "subject": "",
      "body": ""
    }
  }
  $scope.enviado = false

  $scope.submitContacto = ()->
    FaigaApi.postContacto $scope.modelo.contacto, ()->
      $scope.enviado = true


