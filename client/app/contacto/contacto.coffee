'use strict'

angular.module 'faigaApp'
.config ($stateProvider) ->
  $stateProvider.state 'contacto',
    url: '/contacto'
    parent: 'posts'
    templateUrl: 'app/contacto/contacto.html'
    controller: 'ContactoCtrl'

