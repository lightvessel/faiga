'use strict'

angular.module 'faigaApp'
.config ($stateProvider) ->
  $stateProvider
  .state 'main',
    abstract:true
    templateUrl: 'app/main/main.html'
    controller: 'MainCtrl'
