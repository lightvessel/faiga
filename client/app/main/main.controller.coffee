'use strict'

angular.module 'faigaApp'
.controller 'MainCtrl', ($scope, $http, $rootScope, snapRemote) ->
  $rootScope.$on '$stateChangeStart',
    (event, toState, toParams, fromState, fromParams)->
      snapRemote.close()
