'use strict'

angular.module 'faigaApp'
.run ($rootScope, $state) ->
  $rootScope.$state = $state

.config ($stateProvider)->
  $stateProvider.state 'main_layout',
    parent: 'main'
    abstract: true
    views:
      main_container:
        templateUrl:'app/main_layout/main_container.html'
        controller: 'HeaderCtrl'
      menu:
        templateUrl:'app/main_layout/menu.html'
