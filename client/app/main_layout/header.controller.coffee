'use strict'

angular.module 'faigaApp'
.controller 'HeaderCtrl', ($scope) ->
  $scope.setTitle = (title)-> $scope.title = title
  $scope.goBack = ()->history.back()
