'use strict'

angular.module 'faigaApp'
.controller 'EventosCtrl', ($scope, Section, FaigaApi) ->
  $scope.setSection new Section 'Eventos',
    'descripcion',
    'eventosReuniones',
    '/assets/images/eventos.jpg'

  FaigaApi.findEventos (response)->
    $scope.initPosts(response.data.events)

