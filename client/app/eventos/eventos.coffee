'use strict'

angular.module 'faigaApp'
.config ($stateProvider) ->
  $stateProvider.state 'eventos',
    parent: 'posts'
    url: '/eventos'
    controller: 'EventosCtrl'
