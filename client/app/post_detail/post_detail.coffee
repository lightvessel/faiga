'use strict'

angular.module 'faigaApp'
.config ($stateProvider) ->
  $stateProvider.state 'post_detail',
    parent: 'main_layout'
    url: '/news/:id'
    templateUrl: 'app/post_detail/post_detail.html'
    controller: 'PostDetailCtrl'
    canGoBack: true
