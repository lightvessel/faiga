'use strict'

angular.module 'faigaApp'
.controller 'PostDetailCtrl', ($scope, $stateParams, FaigaApi) ->
  FaigaApi.getPost $stateParams.id,
    (response)->
      $scope.post = response.data.news
      $scope.setTitle $scope.post.NewsSection

