'use strict'

angular.module 'faigaApp'
.config ($stateProvider) ->
  $stateProvider.state 'asesorias',
    url: '/asesorias'
    parent: 'posts'
    templateUrl: 'app/asesorias/asesorias.html'
    controller: 'AsesoriasCtrl'
