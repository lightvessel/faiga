'use strict'

angular.module 'faigaApp'
.controller 'AsesoriasCtrl', ($scope, Section, FaigaApi, $sce) ->
  $scope.setSection new Section 'Asesorías',
    'descripcion',
    'asesorias',
    '/assets/images/asesorias.jpg'

  $scope.initPosts([])

  # Esto idealmente debería ser hecho con una directiva, pero de por sí la integración con
  # el las páginas custom es bastante manual, y las personas que trabajan de ese lado están más
  # acostumbradas a envíar su lógica con jQuery.
  FaigaApi.findAsesorias (response)->
    response.data.asesorias.HTML =  $sce.trustAsHtml(response.data.asesorias.HTML + """
      <script>
        $(document).ready(function () {
            $('.asesDesc').hide();
            $('.asesItem').on('click', function () {
                var $asesDesc = $(this).next('.asesDesc');
                $asesDesc.show();
                $('.asesDesc').not($asesDesc).hide();
            });
        });
      </script>
    """)
    $scope.asesorias = response.data.asesorias
