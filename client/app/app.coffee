'use strict'

angular.module 'faigaApp', [
  'ngResource',
  'ngSanitize',
  'ui.router',
  'ui.bootstrap',
  'snap',
  'checklist-model'
]

.config ($stateProvider, $urlRouterProvider, $locationProvider, snapRemoteProvider) ->
  snapRemoteProvider.globalOptions.disable = 'right'
  $urlRouterProvider
  .otherwise '/'

  $locationProvider.html5Mode true

.factory 'Section', ($q)->
  class Section
    constructor: (@title, @description, @style, @image)->

  Section
