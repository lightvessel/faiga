'use strict'

angular.module 'faigaApp'
.config ($stateProvider) ->
  $stateProvider.state 'busqueda',
    abstract: true
    url: '/busqueda'
    parent: 'posts'
    templateUrl: 'app/busqueda/busqueda.html'
    controller: 'BusquedaCtrl'
