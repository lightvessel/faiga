'use strict'

angular.module 'faigaApp'
.controller 'BusquedaEmpresasCtrl', ($scope, Section, FaigaApi) ->
  $scope.initPosts([])

  $scope.buscarEmpresas = () ->
    $scope.startBusqueda()
    FaigaApi.busquedaEmpresas $scope.filtroBusqueda, (empresas)->
      $scope.empresas = empresas
      $scope.stopBusqueda()
