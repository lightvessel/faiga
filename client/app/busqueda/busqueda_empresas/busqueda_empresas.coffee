'use strict'

angular.module 'faigaApp'
.config ($stateProvider) ->
  $stateProvider.state 'busqueda_empresas',
    url: '/empresas'
    parent: 'busqueda'
    templateUrl: 'app/busqueda/busqueda_empresas/busqueda_empresas.html'
    controller: 'BusquedaEmpresasCtrl'
