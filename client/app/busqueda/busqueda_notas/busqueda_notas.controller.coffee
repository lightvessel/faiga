'use strict'

angular.module 'faigaApp'
.controller 'BusquedaNotasCtrl', ($scope, Section, FaigaApi) ->
  $scope.initPosts([])

  $scope.buscarPosts = () ->
    $scope.startBusqueda()
    FaigaApi.busquedaNotas $scope.filtroBusqueda, (news)->
      $scope.initPosts(news)
      $scope.stopBusqueda()
