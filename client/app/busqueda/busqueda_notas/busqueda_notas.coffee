'use strict'

angular.module 'faigaApp'
.config ($stateProvider) ->
  $stateProvider.state 'busqueda_notas',
    url: '/notas'
    parent: 'busqueda'
    templateUrl: 'app/busqueda/busqueda_notas/busqueda_notas.html'
    controller: 'BusquedaNotasCtrl'
