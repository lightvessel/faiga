'use strict'

angular.module 'faigaApp'
.controller 'BusquedaCtrl', ($scope, Section, FaigaApi) ->
  $scope.setSection new Section 'Búsqueda',
    'descripcion',
    'busqueda',
    '/assets/images/busqueda.jpg'

  $scope.initPosts([])

  $scope.startBusqueda = ->
    $scope.buscando = true
  $scope.stopBusqueda = ->
    $scope.buscando = false
