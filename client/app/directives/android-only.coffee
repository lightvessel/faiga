'use strict'

angular.module 'faigaApp'
.directive 'androidOnly', () ->
  scope: false
  restrict: "A"
  link: (scope, element, attrs) ->
    isAndroid = /(android)/i.test(navigator.userAgent)

    if(isAndroid)
      mostroEstaSemana = Cookies.get("mostroEstaSemana") == "true"
      if(!mostroEstaSemana)
        Cookies.set('mostroEstaSemana', "true", {expires: 7});
        element.show()
