'use strict'

angular.module 'faigaApp'
.directive 'iosOnly', () ->
  scope: false
  restrict: "A"
  link: (scope, element, attrs) ->
    isiOS = /(iPad|iPhone|iPod)/g.test( navigator.userAgent );

    if(isiOS)
      mostroEstaSemana = Cookies.get("mostroEstaSemana") == "true"
      if(!mostroEstaSemana)
        Cookies.set('mostroEstaSemana', "true", {expires: 7});
        element.show()

