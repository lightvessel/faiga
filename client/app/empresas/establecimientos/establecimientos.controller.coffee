'use strict'

angular.module 'faigaApp'
.controller 'EstablecimientosCtrl', ($scope, Section, $stateParams, FaigaApi) ->
  if($stateParams.letra)
    $scope.letraActual = $stateParams.letra.toUpperCase()
    FaigaApi.findEstablecimiento $stateParams.letra, (result)->
      $scope.setEmpresas(result.data.empresas)
  else
    $scope.clearEmpresas()
