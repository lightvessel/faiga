'use strict'

angular.module 'faigaApp'
.config ($stateProvider) ->
  $stateProvider.state 'establecimientos',
    url: '/establecimientos/:letra?'
    parent: 'empresas'
    templateUrl: 'app/empresas/establecimientos/establecimientos.html'
    controller: 'EstablecimientosCtrl'
