'use strict'

angular.module 'faigaApp'
.controller 'EmpresasCtrl', ($scope, Section) ->
  $scope.setSection new Section 'Empresas',
    'descripcion',
    'empresasGeoLocation',
    '/assets/images/empresas.jpg'

  $scope.listado = {empresas:[]}
  $scope.initPosts([])
  $scope.alphabet = "ABCDEFGHIJKLMNÑOPQRSTUVWXYZ"
  $scope.setEmpresas = (empresas)->
    $scope.listado.empresas = empresas
  $scope.clearEmpresas = () ->
    $scope.listado = {empresas:[]}
