'use strict'

angular.module 'faigaApp'
.config ($stateProvider) ->
  $stateProvider.state 'proveedores',
    url: '/proveedores/:letra?'
    parent: 'empresas'
    templateUrl: 'app/empresas/proveedores/proveedores.html'
    controller: 'ProveedoresCtrl'
