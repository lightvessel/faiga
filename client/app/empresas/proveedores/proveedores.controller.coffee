'use strict'

angular.module 'faigaApp'
.controller 'ProveedoresCtrl', ($scope, Section, $stateParams, FaigaApi) ->
  if($stateParams.letra)
    $scope.letraActual = $stateParams.letra.toUpperCase()
    FaigaApi.findProveedores $stateParams.letra, (result)->
      $scope.setEmpresas(result.data.proveedores)
  else
    $scope.clearEmpresas()
