'use strict'

angular.module 'faigaApp'
.config ($stateProvider) ->
  $stateProvider.state 'empresas',
    url: '/empresas'
    parent: 'posts'
    templateUrl: 'app/empresas/empresas.html'
    controller: 'EmpresasCtrl'
