'use strict'

angular.module 'faigaApp'
.config ($stateProvider) ->
  $stateProvider.state 'aporte_empresarial',
    parent: 'posts'
    url: '/aporte-empresarial'
    templateUrl: 'app/aporte_empresarial/aporte_empresarial.html'
    controller: 'AporteEmpresarialCtrl'
