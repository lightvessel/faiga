'use strict'

angular.module 'faigaApp'
.controller 'AporteEmpresarialCtrl', ($scope, Section, FaigaApi) ->
  $scope.setSection new Section 'Aporte Empresarial',
    'descripcion',
    'aporteEmpresarial',
    '/assets/images/aporteEmpresarial.jpg'

  $scope.initPosts([])

