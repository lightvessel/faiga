'use strict'

angular.module 'faigaApp'
.config ($stateProvider) ->
  $stateProvider.state 'aporte_sfgb',
    parent: 'aporte_empresarial'
    url: '/aporte-sfgb'
    templateUrl: 'app/aporte_empresarial/aporte_sfgb/aporte_sfgb.html'
    controller: 'AporteSFGBCtrl'
    canGoBack: true
