'use strict'

angular.module 'faigaApp'
.controller 'AporteSFGBCtrl', ($scope, Section, FaigaApi) ->

  $scope.initPosts([])

  FaigaApi.findAporteSFGB (response) ->
    $scope.aportes = response.data.page
