'use strict'

angular.module 'faigaApp'
.controller 'AporteFATIDACtrl', ($scope, Section, FaigaApi) ->
  $scope.initPosts([])

  FaigaApi.findAporteFATIDA (response) ->
    $scope.aportes = response.data.page
