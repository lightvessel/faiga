'use strict'

angular.module 'faigaApp'
.config ($stateProvider) ->
  $stateProvider.state 'aporte_fatida',
    parent: 'aporte_empresarial'
    url: '/aporte-fatida'
    templateUrl: 'app/aporte_empresarial/aporte_fatida/aporte_fatida.html'
    controller: 'AporteFATIDACtrl'
    canGoBack: true
