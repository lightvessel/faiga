#!/bin/bash

sftp -oPort=5000 faiga.com@faiga.com


cd /htdocs/m

put *
mkdir assets
mkdir app
mkdir bower_components

put -r app
put -r assets
put -r bower_components
