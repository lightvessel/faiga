#!/bin/bash

clean ()
{
   grunt clean
   rm -rf dist
}

build ()
{
  sudo npm install -g bower
  sudo npm install -g grunt-cli
  sudo npm install -g generator-angular-fullstack
 
  #install npm dependencies

  npm install
  bower install

  grunt build:dist
}

show_valid_arguments ()
{
  let $# || echo Invalid Argment supplied [$1]
  let $# || echo Valid argments can be:  # Exit if no arguments!
  let $# || echo - clean: Cleans the build enviroment. grunt clean and removal of the dist folder
  let $# || echo - distributable: Builds the project. Installs grunt, bower and dependencies. Builds project
  let $# || { echo End of helper; exit 1; }  # Exit if no arguments!
}

if [ "$#" -ne 1 ]; then
    echo "Illegal number of parameters"
    exit
fi

case "$1" in
"clean") clean
  ;;
"distributable") build
  ;;
*) show_valid_arguments
    ;;
esac




